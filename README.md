# database fluigDataBase + fluigApp

[![N|Solid](https://fluiggers.com.br/uploads/default/original/1X/7e5f8e38f056a498296cd6a38b305e7736acdeaf.png)](https://fluiggers.com.br)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/fluiggers/fluig-docker)

fluigDataBase is fluiggers repository for fluigApp.

## Tech

fluigDataBase uses a number of open source projects to work properly:

* [Docker](https://www.docker.com/) - The preferred choice for millions of developers that are building containerized apps!
* [MySQL](https://www.mysql.com) - Most popular open source database.

## Installation
Edit mysql-root and mysql-user-pass files to change root`s and fluiggers password in your localhost:
```sh
cd database/secrets
```
```sh
sudo echo -n "root-password-here" > mysql-root-pass
```
```sh
sudo echo -n "fluig-password-here" > mysql-user-pass
```
## Docker
FluigApp is very easy to install and deploy in a Docker container.

By default, the Docker will redirect 3306 port in your local host, so change this within the Dockerfile if necessary. When ready, simply use the Dockerfile to build the image.

First, we need to build de fluigApp image.
The first time you run the build, version 1.7 of fluig will be downloaded and will run the first time, so that the database and folder structure are created.
This may take a while depending on the speed of your connection.
Don't worry, before finalizing the script it will correct the routes for wcmdir and apps and company folders for the created volumes.
```sh
cd fluig-docker/fluigApp
docker build -f fluigApp.dockerfile -t fluiggers/fluigapp .
```
Then use compose to run the two containers:
fluigDataBase
fluigApp

```sh
cd ../
docker-compose up -d
```
This will create and run.

#### Verify the deployment by navigating to your server address in your mysql client.

```sh
mysql -u root -p
```

#### Container shell access and viewing MySQL logs
The docker exec command allows you to run commands inside a Docker container. The following command line will give you a bash shell inside your mysql container:
```sh
docker exec -it fluigApp bash
```
#### The log is available through Docker's container log:
```sh
docker logs fluigApp -f
```

#### Creating database dumps
Most of the normal tools will work, although their usage might be a little convoluted in some cases to ensure they have access to the mysqld server. A simple way to ensure this is to use docker exec and run the tool from the same container, similar to the following:

```sh
docker exec fluigDataBase sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /some/path/on/your/host/all-databases.sql
```
#### Restoring data from dump files
For restoring data. You can use docker exec command with -i flag, similar to the following:

```sh
 docker exec -i fluigDataBase sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < /some/path/on/your/host/all-databases.sql
```

To stop use:

```sh
docker-compose down
```

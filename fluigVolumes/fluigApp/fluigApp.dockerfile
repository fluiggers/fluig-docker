FROM ubuntu
LABEL version="1.0.0" description="Aplicação Fluig" maintainer="Thiago César Matos<thiago.matos@itsolved.com.br>"
RUN apt-get update && apt-get install -y unzip wget lnav git rar unrar nano && apt-get clean
RUN cd / && mkdir files && chmod 777 -R files/ && mkdir /run/secrets && chmod 777 -R /run/secrets && mkdir files/install && chmod 777 -R files/install/ && mkdir certs && chmod 777 -R certs/ && mkdir company && chmod 777 -R company/ && mkdir fluig && chmod 777 -R fluig/ && mkdir drivers && chmod 777 -R drivers/ && mkdir apps && chmod 777 -R apps/ && mkdir wcmdir && chmod 777 -R wcmdir/ && mkdir wcmlocaldir && chmod 777 -R wcmlocaldir/
VOLUME /files
VOLUME /certs
VOLUME /drivers
VOLUME /apps
VOLUME /company
VOLUME /wcmdir
VOLUME /wcmlocaldir
VOLUME /run/secrets
EXPOSE 80
EXPOSE 8080
EXPOSE 443
EXPOSE 8888
EXPOSE 9999
EXPOSE 8983
EXPOSE 5445
EXPOSE 28777-28788
ADD ./drivers/* /drivers
ADD ./install.conf /files/install/install.conf
WORKDIR /fluig
RUN  cd /files/install && wget --no-check-certificate -cO - https://download.totvs.com/DownloadCentral/DownloadCentral?file=%2FFTPTOTVSTECNOLOGIA%2FTECNOLOGIA%2FMIDIA%2FFLUIG%2FFLUIG-1.7.1-210601-LINUX64.ZIP > fluig.zip && unzip -o fluig.zip && rm fluig.zip && ./jdk-64/bin/java -cp fluig-installer.jar com.fluig.install.ExecuteInstall install.conf && rm -R /files/install/*
ADD ./domain.xml /files/install/
ADD ./host.xml /files/install/
ADD docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh && ln -s /usr/local/bin/docker-entrypoint.sh /
RUN mv -f /files/install/*.xml /fluig/appserver/domain/configuration/
ENTRYPOINT "docker-entrypoint.sh"
CMD tail -f /dev/null
